module.exports = function (grunt) {
    "use strict";

    grunt.initConfig({
        json: {
            main: {
                options: {
                    namespace: "spec",
                    processContent: function(content) {
                        let output = {};
                        output["definitions"] = content.definitions;
                        for (var attr in content.terms) {output["definitions"][attr] = content.terms[attr]};
                        output["organisation"] = content.organisation;
                        output["survey"] = content.survey;
                        let result = {};
                        for (var key in content.pages) {
                            let modules = content.pages[key]['modules'];
                            for (var module in modules) {
                                let type = modules[module]['type'];
                                result[type] = {
                                    obj: modules[module]["obj"],
                                    options: modules[module]["options"],
                                    formatter: modules[module]["formatter"]
                                };
                            }
                        };
                        output["modules"] = result;
                        return output;
                    }
                },
                src: ['spec.json'],
                dest: 'src/js/spec.json.js'
            }
        },
        ts: {
            default: {
                src: ["src/\*\*/\*.ts"],
                out: './dist/script.js',
                options: {
                    allowJs: true,
                    experimentalDecorators: true,
                    target: "es3",
                    sourceMap: false,
                    noUnusedLocals: true,
                    noUnusedParameters: true,
                    pretty: false,
                    noEmitHelpers: true
                }
            }
        },
        postcss: {
            options: {
                processors: [
                    require('postcss-easy-import')(),
                    require('postcss-custom-properties')(),
                    require('postcss-custom-selectors')(),
                    require('postcss-custom-media')(),
                    require('postcss-media-minmax')(),
                    require('postcss-color-function')(),
                    require('postcss-assets')({
                        loadPaths: ['src/assets/']
                    }),
                    require('postcss-nesting')(),
                    require('pixrem')({
                        replace: true
                    }),
                    require('postcss-property-lookup')(),
                    require('postcss-calc')({
                        preserve: false,
                        mediaQueries: true,
                        warnWhenCannotResolve: true
                    }),
                    require('autoprefixer')({
                        browsers: 'last 5 versions'
                    }),
                    require('oldie'),
                    require('cssnano')()
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: "./src/css",
                    src: [
                        "*.css",
                        "!_*.css"
                    ],
                    dest: "./dist"
                }]
            }
        },
        pug: {
            compile: {
                options: {
                    client: false,
                    pretty: false,
                    data: function(destination, source) {
                        return require('./spec.json');
                    }
                },
                files: [{
                    src: ["./src/html/*.pug"],
                    dest: "./dist"
                }]
            }
        },
        uglify: {
            options: {
                enclose: true,
                preserveComments: 'some',
                mangle: {
                    toplevel: true,
                    eval: true
                },
                compress: {
                    drop_console: true,
                    dead_code: true,
                    drop_debugger: true,
                    loops: true,
                    unsafe: true,
                    unused: true,
                    properties: true,
                    conditionals: true,
                    pure_getters: true,
                    booleans: true,
                    join_vars: true
                }
            },
            my_target: {
                files: {
                    './dist/script.min.js': ['./dist/script.js']
                }
            }
        },
        watch: {
            json: {
                files: ["spec.json"],
                "tasks": ["json"]
            },
            ts: {
                files: ["src/js/script.ts"],
                tasks: ["ts"]
            },
            pugjs: {
                files: ["src/html/*.pug"],
                tasks: ["pug"]
            },
            postcss: {
                files: ["src/css/*.css"],
                tasks: ["postcss"]
            },
            uglify: {
                files: ["dist/script.js"],
                tasks: ["uglify"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-postcss");
    grunt.loadNpmTasks("grunt-json");
    grunt.loadNpmTasks("grunt-contrib-pug");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-contrib-uglify");

    grunt.registerTask("default", [
        "json",
        "pug",
        "postcss",
        "ts",
        "uglify"
    ]);
}
