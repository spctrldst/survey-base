/// <reference path="typing.d.ts" />
/// <reference path="spec.json.js" />

namespace Spec {
    export const {organisation, survey, definitions, modules, code} = spec["spec"];
}

String.prototype["format"] = (...args): string => {
    /**
     * @author Alex McNeill
     * @since 23-03-2017
     * @description Format function for String to replicate python functionality.
     * @param {Array<string>} args A collection of strings with which to format the string.
     */
    return this.replace(/{(\d+)}/g, function(match, number: number) {
        return typeof args[number] !== 'undefined'? args[number] : match;
    });
}

let $ = (selectors: string): HTMLElement[] => {
    /**
     * @author Alex McNeill
     * @since 23-03-2017
     * @description Dollar function similar to jQuery but without all the crap on top. Backports querySelectorAll for incompatible browsers.
     * @param {string} selectors The CSS selector we want to search for.
     * @returns {Array<Element>} An array of elements that matched the selector.
     */
    if (document.querySelectorAll) {
        return [].slice.call(document.querySelectorAll(selectors));
    } else {
        let style: HTMLStyleElement = document.createElement('style');
        let elements: HTMLElement[] = [];
        document.documentElement.firstChild.appendChild(style);
        document["_qsa"] = [];
        style["styleSheet"].cssText = selectors + '{x-qsa:expression(document._qsa && document._qsa.push(this))}';
        window.scrollBy(0, 0);
        style.parentNode.removeChild(style);
        for (let element of document["_qsa"]) {
            element.style.removeAttribute("x-qsa");
            elements.push(element);
        }
        return elements;
    }
}

let iOS: RegExpMatchArray = navigator.userAgent.match(/iPad|iPhone|iPod/i);
let radio: Object = {};
let checkRadio = (object: any) => {
    let {name, val, checked} = object;
    if (radio[name] === null) {
        radio[name] = val;
    } else {
        if (radio[name] === val) {
            object[checked] = false;
            radio[name] = null;
        } else {
            radio[name] = val;
        }
    }
}

let writeout = (definitions: Object = Spec.definitions, selector: string = "[write=true]") => {
    /**
     * @author Alex McNeill
     * @since 23-03-2017
     * @description Looks up an element's ID in the definitions and writes it to the innerHTML of the object.
     * @param {string} selector A CSS selector to write definitions into.
     * @param {Object} definitions A collection of definitions to look up.
     * @returns
     */
    for (let element of $(selector)) {
        element.innerHTML = definitions[element.id];
        element.className = '';
        element.id = '';
    }
}
writeout();

let setupInputs = (selector: string = "[type=radio]") => {
    for (let input of $(selector) as HTMLInputElement[]) {
        input.onclick = function () {
            checkRadio(this);
        }
    }
}

if (!!document["mrForm"]) setupInputs();

let setupDefinitions = (definitions: Object = Spec.definitions, selector: string = ".definition") => {
    /**
     * @author Alex McNeill
     * @since 23-03-2017
     * @description Adds title hovers to elements matching selector.
     * @param {string} selector A CSS selector to write title hovers to.
     * @param {Object} definitions A collection of definitions to look up.
     * @returns
     */
    for (let element of $(selector)) {
        element.setAttribute('title', definitions[element.id]);
        if (!!iOS) element["style"].cursor = "pointer";
    }
}

setupDefinitions();

let setupHovers = (modules: Object = Spec.modules) => {
    /**
     * @author Alex McNeill
     * @since 23-03-2017
     * @description Sets up title hovers for selected modules.
     * @param {Object} modules A collection of modules to set hovers up for.
     */
    for (let module in modules) {
        for (let item in modules[module]) {
            let {options, formatter, subject} = modules[module][item];
            for (let option of options) {
                let index = options.indexOf(option)+1;
                let results = $("#{0} [value$={1}]"["format"](module, index));
                for (let result of results) {
                    result.setAttribute('title', option["format"](formatter["format"](subject)));
                }
            }
        }
    }
}

setupHovers();

let calcText = (box: HTMLTextAreaElement) => {
    /**
     * @author Alex McNeill
     * @since 23-03-2017
     * @description Calculates the remaining characters left in a box.
     * @param {Element} box A text input element to calculate remaining characters for.
     * @returns
     */
    let count: number = (parseInt(box.getAttribute("maxlength")) || 1024) - box.value.length;
    let plural: string = count !== 1 ? 's' : '';
    let message: string = count < 200 ? "&#187; {0} character{1} remaining."["format"](count, plural) : '';
    return message
}

let setupTextInputs = (selector: string = "textarea") => {
    /**
     * @author Alex McNeill
     * @since 23-03-2017
     * @description Initialises counting for each item matching the selector.
     * @param {string} selector A CSS selector that should match text input elements.
     */
    for (let box of $(selector) as HTMLTextAreaElement[]) {
        let counter: HTMLDivElement = document.createElement('div');
        counter.id = '{0}_counter'["format"](box.name);
        counter.style.display = "none";
        box.parentElement.appendChild(counter);
        if (!!calcText(box)) counter["style"].display = "block";
        box.onkeyup = function () {
            /**
             * @author Alex McNeill
             * @since 23-03-2017
             * @description Binds the counter checker to the onkeyup function of the element.
             */
            let my = this as HTMLTextAreaElement;
            let counter = document.getElementById("{0}_counter"["format"](my.name));
            counter["style"].display = (!!calcText(my) ? "block" : "none");
        }
    }
}

setupTextInputs();
